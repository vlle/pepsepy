# Testing your code

## Automated vs. Manual Testing

To have a complete set of manuals tests, all you need to do is make a list of all features your applications has, the different types of input it can accept, and the expected results [1][1]. Now, every time you make a change to your code, you need to go through every single item on that list and check it. (**not fun**)

Automated testing is the execution of your test plan (the parts of your application you want to test, the order in which you want to test them, and the expected responses) by a script instead of a human.

## Unit Tests vs. Integrating Tests

Testing multiple components is known as **integration testing**.

A major challenge with integration testing is when an integration test doesn’t give the right result. It’s very hard to diagnose the issue without being able to isolate which part of the system is failing.

A **unit test** is a smaller test, one that checks that a single component operates in the right way. A unit test helps you to isolate what is broken in your application and fix it faster.

[1] An integration test checks that components in your application operate with each other.
[2] A unit test checks a small component in your application.

You can write both **integration tests** and **unit tests** in Python. To write a unit test for the built-in function sum(), you would check the output of sum() against a known output.

Ex:

assert sum([1,2,3]) == 6, "Should be 6"

This will not output anything on the REPL because the values are correct.

If the result from sum() is incorrect, this will fail with an AssertionError and the message "Should be 6".

Instead of testing on the REPL, you’ll want to put this into a new Python file called test_sum.py and execute it again:

```python
def test_sum():
    assert sum([1,2,3])==6, "should be 6"

if __name__ == "__main__":
    test_sum()
    print("Everything passed")
```

The **test runner** is a special application designed for running tests, checking the output, and giving you tools for debugging and diagnosing tests and applications.

## Choosing a Test Runner

There are many test runners available for Python. The one built into the Python standard library is called unittest. The three most popular test runners are:

- unittest
- nose or nose2
- pytest

### unittest

unittest contains both a testing framework and a test runner. unittest has some important requirements for writing and executing tests.

unittest requires that:

- You put your tests into classes as methods
- You use a series of special assertion methods in the unittest.TestCase class instead of the built-in assert statement

To convert the earlier example to a unittest test case, you would have to:

~~~Python
import unittest

class TestSum(unittest.TestCase):

    def test_sum(self):
        self.assertEqual(sum([1,2,3]),6,"Should be 6")
    def test_sum_tuple(self):
        self.assertEqual(sum((1,2,2)),6,"Should be 6")

if __name__ == '__main__':
    unittest.main()
~~~

### nose

**nose** is compatible with any tests written using the **unittest** framework and can be used as a drop-in replacement for the unittest test runner. The development of nose as an open-source application fell behind, and a fork called **nose2** was created. If you’re starting from scratch, it is recommended that you use **nose2** instead of nose.

Note: If your application is a single script

You can import any attributes of the script, such as classes, functions, and variables by using the buil-in __import__() function.

The benefit of using __import__() is that you don’t have to turn your project folder into a package, and you can specify the file name. This is also useful if your filename collides with any standard library packages. For example, math.py would collide with the math module.

## How to structure a simple test

Before you dive into writing tests, you’ll want to first make a couple of decisions:

1. What do you want to test?
2. Are you writing a unit test or an integration test?

Then the structure of a test should loosely follow this workflow:

1. Create your inputs
2. Execute the code being tested, capturing the output
3. Compare the output with an expected result

### How to write  Assertions

The last step of writing a test is to validate the output against a known response. This is known as an assertion. There are some general best practices around how to write assertions:

- Make sure tests are repeatable and run your test multiple times to make sure it gives the same result every time
- Try and asserts that relate to your input data, such as checking the result is the actual sum of values in the sum() example

unittest comes with lots of methods to assert on the values, types, and existence of variables. Here are some of the most commonly used methods:

|Method|Equivalent to|
|------|-----------------|
|.assertEqual(a, b)|a == b|
|.assertTrue(x)|bool(x) is True|
|.assertFalse(x)|bool(x) is False|
|.assertIs(a, b)|a is b|
|.assertIsNone(x)|x is None|
|.assertIn(a, b)|a in b|
|.assertIsInstance(a, b)|isinstance(a, b)|

.assertIs(), .assertIsNone(), .assertIn(), and .assertIsInstance() all have opposite methods, named .assertIsNot(), and so forth.

### Side Effects

When you’re writing tests, it’s often not as simple as looking at the return value of a function. Often, executing a piece of code will alter other things in the environment, such as the attribute of a class, a file on the filesystem, or a value in a database. These are known as side effects and are an important part of testing. Decide if the side effect is being tested before including it in your list of assertions.



## Reference

[1]: https://realpython.com/python-testing/